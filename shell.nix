{ pkgs ? import <nixpkgs> {} }:
with pkgs; mkShell {
  name = "samsartor-site";

  shellHook =
  ''
  '';

  packages = [
    nodejs
    yarn
    zola
    unzip
    gnused
    curl
 ];
}
