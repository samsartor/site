import katex from 'katex';
import 'katex/dist/katex.css';
import { icoInit } from './ico';
import { ColorDab } from "./colordab";

const a_lower = 'a'.charCodeAt(0);
const a_upper = 'A'.charCodeAt(0);

function rot13(x: string) {
    let y = ''
    for (let c of x.split('')) {
        let i = c.charCodeAt(0);
        if (a_lower <= i && i <= a_lower + 26) {
            i = (i - a_lower + 13) % 26 + a_lower;
        } else if (a_upper <= i && i <= a_upper + 26) {
            i = (i - a_upper + 13) % 26 + a_upper;
        }
        y += String.fromCharCode(i);
    }
    return y;
}

function nospamOff(this: HTMLElement, event: Event) {
    this.removeEventListener('click', nospamOff);
    this.removeEventListener('mouseover', nospamOff);
    let href = this.attributes.getNamedItem('href');
    if (href != null) {
        href.value = rot13(href.value);
    }
}

function siteInit() {
    const math_elements = Array.from(document.getElementsByClassName('math'));
    for (let elem of math_elements) {
        const source = elem.textContent;
        if (source == null || !(elem instanceof HTMLElement)) {
            continue;
        }
        elem.textContent = '';
        try {
            katex.render(source, elem, {
                displayMode: elem.tagName === 'DIV',
            });
        } catch (error) {
            elem.textContent = source;
            console.error(error);
        }
    }

    const nospam_elements = Array.from(document.getElementsByClassName('nospam'));
    for (let elem of nospam_elements) {
        elem.addEventListener('mouseover', nospamOff);
        elem.addEventListener('click', nospamOff);
    }
}

customElements.define('col-s', ColorDab);

siteInit();
icoInit();
