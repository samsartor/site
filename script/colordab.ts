import Color from 'colorjs.io/dist/color';
import cie_xyz from './spectrum.json?raw';

function wavelengthToColor(nm: number) {
  return new Color('XYZ', JSON.parse(cie_xyz)[Math.round(nm) + '']);
}

function kelvinToColor(temperature: number, Y: number = 1.0) {
  // Thanks Perplexity!
  // https://www.perplexity.ai/search/find-a-csv-or-json-translating-rez82t_uS42wAo1ScAs4sQ

  let x, y;

  // Ensure temperature is within the valid range
  temperature = Math.max(1667, Math.min(25000, temperature));

  if (temperature <= 4000) {
    // For temperatures <= 4000K
    x = -0.2661239 * Math.pow(10, 9) / Math.pow(temperature, 3)
      - 0.2343589 * Math.pow(10, 6) / Math.pow(temperature, 2)
      + 0.8776956 * Math.pow(10, 3) / temperature
      + 0.179910;
  } else {
    // For temperatures > 4000K
    x = -3.0258469 * Math.pow(10, 9) / Math.pow(temperature, 3)
      + 2.1070379 * Math.pow(10, 6) / Math.pow(temperature, 2)
      + 0.2226347 * Math.pow(10, 3) / temperature
      + 0.240390;
  }

  if (temperature <= 2222) {
    // For temperatures <= 2222K
    y = -1.1063814 * Math.pow(x, 3)
      - 1.34811020 * Math.pow(x, 2)
      + 2.18555832 * x
      - 0.20219683;
  } else if (temperature <= 4000) {
    // For temperatures between 2222K and 4000K
    y = -0.9549476 * Math.pow(x, 3)
      - 1.37418593 * Math.pow(x, 2)
      + 2.09137015 * x
      - 0.16748867;
  } else {
    // For temperatures > 4000K
    y = 3.0817580 * Math.pow(x, 3)
      - 5.87338670 * Math.pow(x, 2)
      + 3.75112997 * x
      - 0.37001483;
  }


  const X = (x * Y) / y;
  const Z = ((1 - x - y) * Y) / y;
  return new Color('xyz', [X, Y, Z]);
}


const BGR = new Color.Space({
  id: "bgr",
  name: "BGR",
  base: Color.spaces.srgb,
  coords: {
    b: {
      range: [0, 1],
      name: "Blue",
    },
    g: {
      range: [0, 1],
      name: "Green",
    },
    r: {
      range: [0, 1],
      name: "Red",
    },
  },
  toBase([b, g, r]) {
    return [r, g, b];
  },
  fromBase([r, g, b]) {
    return [b, g, r];
  },
});
Color.Space.register(BGR);

const CMY = new Color.Space({
  id: "cmy",
  name: "CMY",
  base: Color.spaces.srgb,
  coords: {
    c: {
      range: [0, 1],
      name: "Cyan",
    },
    m: {
      range: [0, 1],
      name: "Magenta",
    },
    y: {
      range: [0, 1],
      name: "Yellow",
    },
  },
  toBase([c, m, y]) {
    return [1 - c, 1 - m, 1 - y];
  },
  fromBase([r, g, b]) {
    return [1 - r, 1 - g, 1 - b];
  },
});

Color.Space.register(CMY);

let global_color_dabs: Set<ColorDab> = new Set();

export class ColorDab extends HTMLElement {
  static observedAttributes = ['value', 'space', 'weights', 'magnitude'];

  public value: string = '#ff00ff';
  public space: string = 'sRGB';
  public weights: Array<number> | undefined = undefined;
  public magnitude: number = 1;
  public color: Color = new Color('rec2020', [0, 0, 0]);

  public interactiveControls: HTMLElement | null = null;

  constructor() {
    super();
    this.attachShadow({ mode: 'open' });
    this.render();
  }

  attributeChangedCallback(name: string, oldValue: any, newValue: any) {
    this.render();
  }

  connectedCallback() {
    global_color_dabs.add(this);
    this.addEventListener('mouseenter', this.showInteractiveControls);
  }

  disconnectedCallback() {
    global_color_dabs.delete(this);
    this.removeEventListener('mouseenter', this.showInteractiveControls);
  }

  render() {
    this.value = this.getAttribute('value') ?? this.textContent!.trim();
    this.magnitude = parseFloat(this.getAttribute('magnitude') ?? '1');
    this.space = this.getAttribute('space') ?? 'srgb';
    this.weights = this.getAttribute('weights')?.split('+').map(parseFloat);

    let style = `
    <style>
      :host {
        display: inline-flex;
        position: relative;
        align-items: center;
      }
      :host-context(.--inverted) rect { stroke: black; }
      :host-context(div) rect { stroke: white; }
      .interactive-controls {
        color: black;
        display: none;
        position: absolute;
        left: 0;
        top: 100%;
        z-index: 1000;
        width: 100%;
        justify-content: center;
      }
      input, select {
        border: none;
        border-radius: 0;
        margin-top: 3px;
        padding: 8px;
      }
    </style>
    `;
    let valueControls = `
    <label>
      <input type="text" id="valueInput" value="${this.value}">
    </label>
    `;
    let spaceControls = `
    <label>
      <select id="spaceInput">
        <option value="srgb" ${this.space === 'srgb' ? 'selected' : ''}>sRGB</option>
        <option value="bgr" ${this.space === 'bgr' ? 'selected' : ''}>BGR</option>
        <option value="hsl" ${this.space === 'hsl' ? 'selected' : ''}>HSL</option>
        <option value="cmy" ${this.space === 'cmy' ? 'selected' : ''}>CMY</option>
        <option value="xyz" ${this.space === 'xyz' ? 'selected' : ''}>XYZ</option>
        <option value="rec2020" ${this.space === 'rec2020' ? 'selected' : ''}>Rec2020</option>
        <option value="p3" ${this.space === 'p3' ? 'selected' : ''}>P3</option>
      </select>
    </label>
    `;

    try {
      const values = this.value.split('+');
      let width = 1;
      let kind = 'swatch';
      let waves = new Array(100);
      let maximize = false;
      waves.fill(0.0);
      this.color = new Color('rec2020', [0, 0, 0]);
      for (const [i, value] of values.entries()) {
        let phase = [0, Math.PI/2][i % 2];
        let color = null;
        if (value.endsWith('K')) {
          color = kelvinToColor(parseFloat(value.replace(',', '').slice(0, value.length-1)));
          maximize = true;
          spaceControls = '';
        } else if (value.endsWith('nm')) {
          kind = 'wave';
          let wavelength = parseFloat(value.slice(0, value.length-2));
          color = wavelengthToColor(wavelength);
          maximize = true;
          spaceControls = '';
          for (let j = 0; j < waves.length; j++) {
            let theta = j / waves.length / wavelength * 3000 * Math.PI + phase;
            waves[j] += Math.sin(theta);
          }
        } else {
          if (value.startsWith('[') && value.endsWith(']')) {
            let values = value.slice(1, value.length-1).split(',');
            let coords: [number, number, number] = [0, 0, 0];
            const space = Color.Space.get(this.space);
            for (let j = 0; j < 3; j++) {
              const meta = Object.values(space.coords)[j];
              coords[j] = parseFloat(values[j]) * (meta.range ?? meta.refRange ?? [0, 1])[1];
            }
            color = new Color({'space': space, 'coords': coords})
          } else {
            color = new Color(this.value.replace(' ', ''));
            spaceControls = '';
          }
        }
        for (let j = 0; j < 3; j++) {
          let w = 1.0;
          if (this.weights !== undefined) {
            w = this.weights[i];
          }
          this.color.xyz[j] += w * color.xyz[j];
        }
      }

      if (maximize) {
          const m = Math.max(...this.color.srgb_linear);
          for (let j = 0; j<3; j++) {
            this.color.srgb_linear[j] /= m;
          }
      }

      const colorStr = this.color.display();
      let svg = `<rect width="1" height="1" fill="${colorStr}" stroke-width="0.05"/>`;
      if (kind == 'wave') {
        width = 2;
        const waveMax = waves.reduce((a, b) => Math.max(a, b), 0.0);
        const pathData = waves.map((y, index) => {
          const xPos = index / waves.length * width;
          const yPos = this.magnitude * y / waveMax * 0.5 + 0.5;
          return `${index === 0 ? 'M' : 'L'} ${xPos} ${yPos}`;
        }).join(' ');
        svg = `<path d="${pathData}" fill="none" stroke="${colorStr}" stroke-width="0.2"/>`
      }

      this.shadowRoot!.innerHTML = `
      ${style}
      ${this.innerHTML!.trim()}&thinsp;
      <svg xmlns="http://www.w3.org/2000/svg" width="${width}em" height="1em" overflow="visible" viewBox="0 0 ${width} 1">
        ${svg}
      </svg>
      <div class="interactive-controls">
        ${valueControls}
        ${spaceControls}
      </div>
      `;
      this.setupInteractiveControls(true);
    } catch (e) {
      console.error(this, e);
      this.shadowRoot!.innerHTML = `
      ${style}
      ${this.innerHTML}
      <div class="interactive-controls">
        ${valueControls}
        ${spaceControls}
      </div>
      `;
      this.setupInteractiveControls(false);
    }
  }

  setupInteractiveControls(valid: boolean) {
    this.interactiveControls = this.shadowRoot!.querySelector('.interactive-controls');
    const valueInput = this.shadowRoot!.getElementById('valueInput') as HTMLInputElement;
    const spaceInput = this.shadowRoot!.getElementById('spaceInput') as HTMLSelectElement;

    valueInput?.addEventListener('change', (e) => {
      this.value = (e.target as HTMLInputElement).value;
      this.setAttribute('value', this.value);
      this.render();
    });
    if (!valid) {
      valueInput?.setAttribute('style', 'color: red');
    }

    spaceInput?.addEventListener('change', (e) => {
      this.space = (e.target as HTMLSelectElement).value;
      this.setAttribute('space', this.space);
      this.render();
    });

    // Add event listener to hide controls when clicking outside
    document.addEventListener('click', this.hideInteractiveControls);
  }

  showInteractiveControls = () => {
    for (let other of global_color_dabs) {
      if (other.interactiveControls != null) {
        other.interactiveControls.style.display = 'none';
      }
    }

    if (this.interactiveControls) {
      this.interactiveControls.style.display = 'flex';
    }
  }

  hideInteractiveControls = (event: MouseEvent) => {
    if (this.interactiveControls && !this.contains(event.target as Node)) {
      this.interactiveControls.style.display = 'none';
    }
  }
}

window['Color'] = Color;
