const theme_storage_key = 'make_my_eyes_bleed';
let force_theme = false;
window['inverted'] = false;
window.addEventListener('load', init);
preInit();

function setInvert(inverted: boolean, save: boolean = false) {
    if (!force_theme || save) {
        window['inverted'] = inverted;
        if (inverted) {
            document.documentElement.classList.add('--inverted');
        } else {
            document.documentElement.classList.remove('--inverted');
        }
    }
    if (save) {
        force_theme = true;
        localStorage.setItem(theme_storage_key, inverted ? '1' : '0');
    }
}

function preInit() {
    const saved = localStorage.getItem(theme_storage_key);
    if (saved == '1') {
        setInvert(true, true);
    } else if (saved == '0') {
        setInvert(false, true);
    }

    const prefers_light = window.matchMedia('(prefers-color-scheme: light)');
    setInvert(prefers_light.matches);
    prefers_light.addEventListener('change', m => setInvert(m.matches));

    document.fonts.addEventListener('loadingdone', function (event) {
        for (let face of this) {
            if (face.family.includes('NerdFontsSymbols')) {
                document.body.classList.remove('--noicons');
            }
        }
    });
}

function init() {
    const toggle = document.getElementById('darkness-toggle');
    if (toggle) {
        toggle.addEventListener('click', () => setInvert(!window['inverted'], true));
    }
    document.addEventListener('keydown', m => {
        if (m.code == 'Space') {
            setInvert(!window['inverted'], true);
            m.preventDefault();
        }
    });
}
