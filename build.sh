#!/usr/bin/env sh

export PARCEL_WORKER_BACKEND=process

set -ex

yarn install
$(yarn bin)/vite build
zola build --drafts
