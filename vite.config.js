import { defineConfig } from "vite";

export default defineConfig({
    publicDir: false,
    build: {
        outDir: "./static/scripts",
        lib: {
            entry: {
                earlysite: "./script/earlysite.ts",
                site: "./script/site.ts",
            },
            formats: ["es"],
        },
    },
    esbuild: {
        minifyIdentifiers: false,
    }
});
