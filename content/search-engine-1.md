+++
title = "How to Write a Search Engine (for people who like math)"
description = ""
date = 2019-09-08
draft = false
+++

Writing a search engine is hard. When confronted with the endless complexities
of language, it is usually best to use code written by other, presumably
smarter, people. For whatever reason, I was recently forced to implement my own
search engine. Although this might have been accomplished with sufficient
guesswork, I decided to try and derive an appropriate algorithm with *math*!
I've documented my approach here if anyone else ever finds themselves in a
similar situation.

<!-- more -->

Before we begin, we need some data to search. I am endlessly annoyed that
searching "git" on my phone doesn't bring up either the "OctoDroid" or "LabCoat"
apps, so let's reinvent my app drawer.

{{ image(asset="bad-phone-search.png", size="l") }}

# Constructing a Model

I use my app drawer's search bar when I want to find and open a specific app
that is otherwise buried in a sea of little circle icons. So the drawer should
really be trying to guess what app I want to open. Apps that are more likely
should come first and apps that are not at all likely should be left out
entirely.

Before I type a single letter, our search algorithm can already make some
reasonable guesses. Most of the time I am just going to select Firefox, Google
Maps, or Reddit. Those apps should start out at the top of the drawer. Only once
I type "g" should git-related apps start moving up.

## Conditional Probability

The probability that I select an app {{var(name="A")}}, before typing anything,
is called the "prior probability" and written {% math() %} P(A) {% end %}. Once
we have some more evidence of my intentions (in the form of me typing some
search query {{var(name="Q")}}), we want to use use the probability of
"{{var(name="A")}} given that evidence" which is written as {% math() %} P(A
\mid Q) {% end %}. This is called [conditional
probability](https://en.wikipedia.org/wiki/Conditional_probability). So, how do
we calculate conditional probability? In this case, we will use a little recipe
called Bayes' Rule, which says that:

{% math(block=true) %}
\begin{aligned}
P(A \mid Q) = \frac{P(Q \mid A) \; P(A)}{P(Q)}
\end{aligned}
{% end %}

We already know how to estimate {% math() %} P(A) {% end %} (the prior
probability that I am looking for {{var(name="A")}} no matter the query). Simply
track how often I open each app.

But what is {% math() %} P(Q) {% end %}? That's the probability of me typing
some query {{var(name="Q")}} no matter the app I'm looking for and it is a
little tricky to estimate. Thankfully, we can come back to it later since it
isn't needed to simply rank apps relative to each other. {% math() %} P(Q) {%
end %} depends on the query only and so effects the probability of {% math() %}
A \mid Q {% end %} for each app {{var(name="A")}} equally.

So all we need now is {% math() %} P(Q \mid A) {% end %}! What is {% math() %}
P(Q \mid A) {% end %}? Well, if {% math() %} P(A \mid Q) {% end %} is the
probability of me looking for {{var(name="A")}} given I have typed
{{var(name="Q")}}, then {% math() %} P(Q \mid A) {% end %} must be the
probability of me typing {{var(name="Q")}} given I am looking for
{{var(name="A")}}. To calculate this probability, let's think through a
hypothetical sequence of events:

1. I am looking for "LabCoat"
2. Since "LabCoat" is an app for managing git repositories, I think "git"
3. I start by typing the letters "gi"

There are now a bunch more probabilities we need to deal with:

{% math(block=true) %}
\begin{aligned}
P(A) &= \text{How likely is it that am looking for LabCoat?} \\[1em]
P(K \mid A) &= \text{How likely is it that I will think ``git'' when looking for LabCoat?} \\[1em]
P(Q \mid K) &= \text{How likely is it that I will type ``gi'' when thinking ``git''?} \\[1em]
\end{aligned}
{% end %}

Conceptually, typing "gi" is evidence that I am thinking "git" which in turn is
evidence that I am looking for LabCoat. By applying some rules of conditional
probability to Bayes' Rule above (which is left as an exercise for sufficiently
bored readers), we get:

{% math(block=true) %}
\begin{aligned}
P(A \mid Q) = \frac{\sum_K P(Q \mid K) \; P(K \mid A) \; P(A)}{P(Q)}
\end{aligned}
{% end %}

In human-speak, this equation says:

> "The likelihood that I am looking for some app is proportional to the
  similarity of the query to a keyword times the relevance of that keyword to
  the app summed over all relevant keywords."

The term {% math() %} P(K \mid A) {% end %} is fairly easy. It is just a metric
for how relevant {{var(name="K")}} is as a keyword for {{var(name="A")}}. If
LabCoat has {{var(name="n")}} keywords of equal weight, then {% math() %} P(K
\mid A) = \frac{1}{n} {% end %}. If some keyword is stronger, then we can give
it a little more probability and the others a little less.

But it is still a unclear how we should estimate {% math() %} P(Q \mid K) {% end
%}. I did this by borrowing the concept of an "edit" which is some alteration
from one item of text to another.

## Edit Distance

Let's suppose I think the word "firefox" but then type "frefo". To a human it is
pretty easy to guess what happened, but how can a computer guess? If you
remember your college computer science courses you may be screaming "use the the
[Levenshtein distance](https://en.wikipedia.org/wiki/Levenshtein_distance)!" at
your screen. However, the goal of this blog post is to derive a search algorithm
from first principals. So if we want to use the the Levenshtein distance (or a
similar edit-distance metric), we will have to justify it mathematically.

Let's define {{var(name="E")}} as a sequence of edits which is made of
individual alterations {% math() %} E_1, E_2, \dots, E_n {% end %}. The
probability {% math() %} P(E) {% end %} is simply the likelihood of all {%
math() %} E_1, E_2, \dots, E_n {% end %} occurring. Some alteration {% math() %}
E_i {% end %} might be very likely if it is just me leaving some letters off the
end of a word (afte al, typin is so borin) but less likely if it is something
like replacing "i" with "q".

But how does {% math() %} P(E) {% end %} relate to {% math() %} P(Q \mid K) {%
end %}? Let's define {% math() %} P(Q \mid K, E) {% end %} which is the
probability of me typing {{var(name="Q")}} when I meant to type
{{var(name="K")}} but made edits {{var(name="E")}}. If {{var(name="E")}} does
indeed change {{var(name="K")}} into {{var(name="Q")}}, then the probability is
100%. *When those edits are applied to that keyword it will always result in the
query.* Otherwise the probability is 0.

Now from the rules of probability, we can get:

{% math(block=true) %}
P(Q \mid K) = \sum_E P(Q \mid K, E) \; P(E)
{% end %}

See how the term {% math() %} P(Q \mid K, E) {% end %} just acts like a filter,
so that we sum only the probabilities of all edits that change {{var(name="K")}}
into {{var(name="Q")}}? For example, to calculate the probability that we type
"firefox" as "frefo", we sum the probabilities of:

- Deleting the "i" and the "x".
- Deleting "ox" and replacing "iref" with "refo".
- Inserting an "f", deleting "fo", and deleting "x".
- ...

But conceptually this equation calculates a sum, not the minimum, which prevents
us from using the [traditional algorithm for edit
distance](https://en.wikipedia.org/wiki/Wagner%E2%80%93Fischer_algorithm). And
these aren't really distances (1 edit, 2 edits, etc) at all but probabilities
(1%, 30%, etc). What is going on?

## Negative Log

Let's take a break from math and think about how we might implement this
algorithm so far (an exercise I may attempt in a later post). Throughout this
process we keep multiplying small probabilities together to get even smaller
probabilities. Like, what is the chance of replacing "iref" with "refo", out of
all the mistakes I could possibly make? Even randomly forgetting an "i" is
pretty unlikely. And with all the apps on my phone, how often do I open an app
to, say, change my wallpaper? We are running into dangerous
floating-point-precision territory here. The probabilities of making specific
queries could be on the order of {% math() %} 10^{-8} {% end %} or less for
anything but a perfect match.

Maybe we should be using exponential notation? We could take the negative log of
each probability. So instead of saying {% math() %} P(E) = 0.000000001 {% end %}
we will say {% math() %} -\log(P(E)) = I(E) = 8 {% end %}. Because of the
precision benefits, using [log
probability](https://en.wikipedia.org/wiki/Log_probability) is a pretty common
practice in computer science. The notation of {{var(name="I")}} instead of
{{var(name="P")}} as comes from information theory, where the log probability is
called "information content" or "surprise". Information theorists would also tell
us that if we use base 2, our log probabilities will be in units of bits! But
what does all that do to our math?

{% math(block=true) %}
\begin{aligned}
I(E) &= I(E_1 \wedge E_2 \wedge \dots \wedge E_n) \\[1em]
&= -\log[P(E_1) \times P(E_2) \times \dots \times P(E_n)] \\[1em]
&= I(E_1) + I(E_2) + \dots + I(E_n) \\[1em]
&= \text{sum of penalties for each individual alteration} \\[1em]
&= \text{the ``distance'' of } E
\end{aligned}
{% end %}

Cool! Products become sums and we are one step closer to using the edit
distance. And what happens to sums like {% math() %} P(Q \mid K) = \sum_E P(Q
\mid K, E) P(E) {% end %}? Unfortunately there is no simple way to do the log of
a summation. But what are the terms in that summation? The probabilities of
different, increasingly convoluted edits. As the edits get more convoluted they
require exponentially more unlikely alterations. So most of the edits in that
sum are massively less probable than the most probable edit and contribute
little to the total. In fact, the log of a sum is used in machine learning as a
[soft approximation of maximum](https://en.wikipedia.org/wiki/LogSumExp). Thus,
the log of this sum is well approximated by the log probability of the maximally
probable edit (the one with the lowest log probability):

{% math(block=true) %}
I(Q \mid K) \approx \min_E \; I(E) \approx \min_E \; I(E_1) + I(E_2) + \dots + I(E_n)
{% end %}

Voilà, {% math() %} I(Q \mid K) {% end %} is well approximated by the minimum
edit distance between {{var(name="Q")}} and {{var(name="K")}}!  This
approximation works well for other sums in our algorithm. So long as we assume
each query is much closer to one keyword than to the others, we now have:


{% math(block=true) %}
\begin{aligned}
I(A \mid Q) \approx \min_{K, E} \; I(E_1) + I(E_2) + \dots + I(E_n) + I(K \mid A) + I(A) - I(Q)
\end{aligned}
{% end %}

And as discussed above, we can leave out the {% math() %} P(Q) {% end %} term if
all we need is a relative score. At last, we have a mathematically derived
scoring function for our search algorithm:

{% math(block=true) %}
\texttt{score} (A, Q) = \min_K \; \texttt{edit\_distance} (K, Q) + I(K \mid A) + I(A)
{% end %}
