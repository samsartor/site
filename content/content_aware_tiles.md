+++
title = "Content-aware Tile Generation using Exterior Boundary Inpainting"
description = "A flexible training-free diffusion-base method for generating tileable image sets, including self-tiling images, stochastic self-tiling images, and Wang tile textures."
date = 2024-12-03

[extra]
paper = true
authors = "Sam Sartor, Pieter Peers"
publisher_short = "ACM Transactions on Graphics"
publisher_link = "https://doi.org/10.1145/3687981"
paper_link = "https://bin.samsartor.com/content_aware_tiles.pdf"
code_link = "https://github.com/samsartor/content_aware_tiles"
video_link = "https://bin.samsartor.com/content_aware_tiles.mov"
title_length = "l"
plain = true
tilings = true
+++

<tiling-canvas src="https://bin.samsartor.com/content_aware_tile_supplemental/orange_lily/orange_lily.dual_tiles.jpg" mode="dual" class="jumbotron"></tiling-canvas>

<tiling-canvas src="https://bin.samsartor.com/content_aware_tile_supplemental/misty_mountains/misty_mountains.dual_tiles.jpg" mode="dual" class="jumbotron"></tiling-canvas>

<tiling-canvas src="https://bin.samsartor.com/content_aware_tile_supplemental/corks/corks.dual_tiles.jpg" mode="dual" class="jumbotron"></tiling-canvas>

<a class="center_pill" href="https://bin.samsartor.com/content_aware_tile_supplemental/index.html">More Examples</a>

# Abstract

We present a novel and flexible learning-based method for generating
tileable image sets.  Our method goes beyond simple self-tiling,
supporting sets of mutually tileable images that exhibit a high
degree of diversity.  To promote diversity we decouple structure
from content by foregoing explicit copying of patches from an
exemplar image.  Instead we leverage the prior knowledge of natural
images and textures embedded in large-scale pretrained diffusion
models to guide tile generation constrained by exterior boundary
conditions and a text prompt to specify the content. By carefully
designing and selecting the exterior boundary conditions, we can
reformulate the tile generation process as an inpainting problem,
allowing us to directly employ existing diffusion-based inpainting
models without the need to retrain a model on a custom training set.
We demonstrate the flexibility and efficacy of our content-aware
tile generation method on different tiling schemes, such as Wang
tiles, from only a text prompt.  Furthermore, we introduce a novel
Dual Wang tiling scheme that provides greater texture continuity and
diversity than existing Wang tile variants.

# Citation

```bibtex
@conference{Sartor:2024:CAT,
    author    = {Sartor, Sam and Peers, Pieter},
    title     = {Content-aware Tile Generation using Exterior Boundary Inpainting},
    month     = {December},
    year      = {2024},
    booktitle = {ACM Transactions on Graphics},
}
```
