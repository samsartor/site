+++
title = "MatFusion: A Generative Diffusion Model for SVBRDF Capture"
description = "SVBRDF estimation from photographs for three different lighting conditions (directional, natural, and flash/no-flash illumination) is shown by refining a novel SVBRDF diffusion backbone model, named MatFusion."
date = 2023-12-12
draft = false

[extra]
paper = true
authors = "Sam Sartor, Pieter Peers"
publisher_short = "SIGGRAPH Asia 2023"
publisher_link = "https://dl.acm.org/doi/abs/10.1145/3610548.3618194"
paper_link = "https://bin.samsartor.com/matfusion.pdf"
code_link = "https://github.com/samsartor/matfusion"
video_link = "https://bin.samsartor.com/matfusion_fastforward.mov"
title_length = "l"
plain = true
+++

{{ image(asset="matfusion_teaser.png", size="s") }}

# Abstract

We formulate SVBRDF estimation from photographs as a diffusion task. To model the distribution of spatially varying materials, we first train a novel unconditional SVBRDF diffusion backbone model on a large set of 312,165 synthetic spatially varying material exemplars. This SVBRDF diffusion backbone model, named MatFusion, can then serve as a basis for refining a conditional diffusion model to estimate the material properties from a photograph under controlled or uncontrolled lighting. Our backbone MatFusion model is trained using only a loss on the reflectance properties, and therefore refinement can be paired with more expensive rendering methods without the need for backpropagation during training. Because the conditional SVBRDF diffusion models are generative, we can synthesize multiple SVBRDF estimates from the same input photograph from which the user can select the one that best matches the users' expectation. We demonstrate the flexibility of our method by refining different SVBRDF diffusion models conditioned on different types of incident lighting, and show that for a single photograph under colocated flash lighting our method achieves equal or better accuracy than existing SVBRDF estimation methods.

# Citation

```bibtex
@conference{Sartor:2023:MFA,
    author    = {Sartor, Sam and Peers, Pieter},
    title     = {MatFusion: a Generative Diffusion Model for SVBRDF Capture},
    month     = {December},
    year      = {2023},
    booktitle = {ACM SIGGRAPH Asia Conference Proceedings},
}
```
