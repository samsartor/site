+++
title = "Abstract Modules I"
description = ""
date = 2019-04-22
draft = true
+++

In the Rust programming language, items (types, functions, imports, etc.) can appear in modules, `impl`s, and traits.
<!-- more -->

```rust
impl Foo {
    pub type Ok = ();
    pub type Err = String;

    pub static thing: &str = "hello";

    pub fn complex<T>(self, item: T) -> Result<Self::Ok, Self::Err> {
        ...
    }

    pub fn bar(&self) -> Bar {
        ...
    }
}
```

```rust
trait FooLike {
    type Ok;
    type Err;

    pub static thing: &str;

    fn complex<T>(self, item: T) -> Result<Self::Ok, Self::Err>;
    fn bar(&self) -> Bar
}
```

```rust
abstract mod FooLikeImpl {
    type Self;

    mod Bar {
        23
    }

    impl Self {
        type Ok;
        type Err;

        pub static thing: &str;

        fn complex<T>(self, item: T) -> Result<Self::Ok, Self::Err>;
        fn bar(&self) -> Bar;
    }
}

trait FooLike = FooLikeImpl::Self;

struct dyn FooLikeImpl {
    pub Self::thing: &'static str,
    Self::__destructor: /* compiler magic */,
    Self::__align: usize,
    Self::__size: usize,
    Self::bar: fn(*const ()) -> Bar;
}


```
