+++
title = "What is color?"
description = ""
date = 2024-09-27
draft = true
+++

This is absolutely my favorite question, because it intersects so many fields of study! Color means something different to people doing physics, chemistry, biology, mathematics, engineering, psychology, linguistics, or art.

# Physics
> **Color is a property of light. Different colors are different frequencies of light across the visible spectrum.**

{{ svg(asset="what-is-color/electromagnetic_spectrum.svg", size="ml", by_title="Electromagnetic radiation spectrum", by_link="https://commons.wikimedia.org/wiki/File:Elec4tromagnetic_radiation_spectrum_-_en.svg", by_author="Philip Ronan", by_license="CC BY-SA 2.5", by_mod="added dark theme support") }}

For example, light with a wavelength of <col-s>460nm</col-s> is blue. Light can also be mixed, so the combination of light at <col-s>656nm</col-s>, <col-s>486nm</col-s>, <col-s>434nm</col-s>, and <col-s>410nm</col-s> (the [Balmer series](https://en.wikipedia.org/wiki/Balmer_series)) produces the [lovely pink glow of a fusion reactor](https://www.helionenergy.com/)<col-s value="656nm+486nm+434nm+410nm" weights='3+1+0.5+0.1'></col-s>. And all the frequencies together make white. In complete darkness there is no color.

Mixed light is rarely made up of just a couple wavelengths. Physicists generally talk about the [spectral power distribution (SPD)](https://en.wikipedia.org/wiki/Spectral_power_distribution) of light, which measures for _every_ wavelength the amount of power carried at that wavelength. Notably, hot objects around 2200°F (1500K) start to glow red, but not just at <col-s>650nm</col-s>. The SPD of <col-s>1500K</col-s> light covers a wide range of wavelengths but is strongest in red. As the temperature increases, the strongest wavelength gets bluer but the range gets wider. The surface of the sun is <col-s>6500K</col-s> (sorta), where the SPD is nearly flat across the visual spectrum. That is daylight! Past <col-s>10,000K</col-s> the color starts to look blue. Old lightbulbs were labeled with the temperature of their filament, and so people now associate temperatures in the range 1000-10,000K with those colors.

# Chemistry
> **Color is a property of pigments. Different pigments absorb different amounts of each frequency of light.**

{{ image(asset="what-is-color/Indian_pigments.jpg", size="ml", by_title="Indian pigments", by_link="https://en.wikipedia.org/wiki/File:Indian_pigments.jpg", by_author="Dan Brady", by_license="CC BY 2.0", border="true") }}

There are many more colors of pigment than colors of light. A blue pigment is one that reflects mostly <col-s>460nm</col-s> of light, absorbing all longer or shorter wavelengths. But what about black? <col-s value="black">Black pigments</col-s> obviously exist, but there is no black wavelength of light. There is no black stripe in the rainbow. Black is a _substance_, some material that absorbs light of any frequency. In absolute darkness or direct sunlight, a black pigment is still black. There is also [no such thing as brown light, because brown is just "dark orange"](https://www.youtube.com/watch?v=wh4aWZRtTwU). A <col-s value="588nm" magnitude="0.2">small amount of orange light</col-s> is still orange, not brown. But <col-s value="#3f2108">brown</col-s> pigments are all over the place, they absorb more light overall when compared to an <col-s>orange</col-s> pigment making them "darker".

When mixing pigments together, the absorption spectra combine. [To get a <col-s>green</col-s> pigment you generally need to mix <col-s>cyan</col-s> with <col-s>yellow</col-s>](https://en.wikipedia.org/wiki/CMYK_color_model). The cyan pigment will absorb the redish wavelengths and the yellow pigment will absorb the bluish wavelengths, leaving only the green. If you mix <col-s>blue</col-s> and <col-s>yellow</col-s> pigments, the blue pigment can absorb most of the green light often leaving you with a <col-s value="#231c05">mucky blackish</col-s> color. This is subtly different from mixing wavelengths of light. If you mix <col-s value="460nm">blue</col-s> and <col-s value="572nm">yellow</col-s> light, they add to give a <col-s value="572nm+460nm">light purple</col-s>.

# Biology
> **Color is the stimulation of cone cells. There are three different colors of cone.**

{{ svg(asset="what-is-color/color_sensitivity.svg", size="ml", by_title="1416 Color Sensitivity", by_link="https://commons.wikimedia.org/wiki/File:1416_Color_Sensitivity.svg", by_author="Francois~frwiki", by_license="CC BY-SA 4.0", by_mod="added dark theme support") }}

<col-s value="460nm">Short wavelengths</col-s> activate the blue cone cells inside your eyes, so they look blue. <col-s value="650nm">Long wavelengths</col-s> activate the red cone cells, so they look red. Green cone cells are sensitive to <col-s value="530nm">wavelengths in the middle</col-s> (kinda).

Because you only have three types of cone cells, [some combinations of light will look the same](https://en.wikipedia.org/wiki/Metamerism_(color)). In physics terms, pure <col-s>492nm</col-s> light and mixed <col-s weights="1.5+1">520nm+460nm</col-s> light are completely different colors. But when looking at them with your biological eyes, they are the same shade of cyan. Both your blue and green cones are sensitive to the single cyan wavelength, but can be activated individually by mixed blue and green light.

We abuse the biology of vision to make screens work. A real rainbow emits all the frequencies of visible light, but a picture of a rainbow only eimits blue-ish, green-ish, and red-ish frequencies. You can't tell the difference.

Biology is also why the violet end of the spectrum looks purplish. As already mentioned, our red cones are mostly sensitive to long wavelengths around <col-s>650nm</col-s>, but respond to very short wavelengths around <col-s>400nm</col-s> as well, making them look purple. The red sensors in most cameras don't respond the same way. [When you look at the edge of a rainbow you see violet but your cellphone only sees blue](https://www.youtube.com/watch?v=HauiF_AQUIY). Making the rainbow into a color wheel requires crossing the ["line of purples"](https://en.wikipedia.org/wiki/Line_of_purples) from spectral violet to spectral red. That the two look similar is complete coincidence.

There are all sorts of interesting things you can learn about color vision. Some of my favorites are:
- In low light conditions, our rod cells contribute more to our vision, completely changing how we perceive color.
- Many people [only have two functioning colors of cone cell](https://en.wikipedia.org/wiki/Congenital_red%E2%80%93green_color_blindness). Depending on which cells are functioning, they might experience <col-s value="#ff5e2c">orange</col-s> and <col-s value="#90ff2d">green</col-s> as nearly the same color (because they activate the same functioning cones) or <col-s value="#374ea1">blue</col-s> and <col-s value="#ff3c22">red</col-s> as nearly the same color.
- A few women [have an additional functioning cone cell](https://en.wikipedia.org/wiki/Tetrachromacy#Tetrachromacy_in_carriers_of_CVD), allowing them to decern combinations of wavelengths which would appear to the rest of us as exactly the same.
- All humans have [an additional cell in our eyes which is sensitive to some other <col-s value="480nm">bluish</col-s> wavelengths](https://en.wikipedia.org/wiki/Intrinsically_photosensitive_retinal_ganglion_cell), but we can't consciously perceive the effect. It only helps to synchronize the circadian rhythm.
- Animals have entirely different sets of cone cells. Many insects can see into the <col-s value="300nm">ultraviolet</col-s> wavelengths, while reptiles can see into the <col-s value="1000nm">infrared</col-s>. Birds often have 5-7 different cone cells, allowing them to decern all sorts of combinations we can't. Famously, mantis shrimp have 12 different cone cells, [but their brains can't really use all that information](https://www.nature.com/articles/nature.2014.14578).
- Your red cones are much more sensitive to light at <col-s>560nm</col-s> than the <col-s>650nm</col-s> wavelengths we think of as red. This is one reason the cones are properly called "long", "medium", and "short". Your perception of green comes not from green-sensitive cones directly, but from ganglion cells behind the cones that subtract the long wavelength stimulus from the medium wavelength stimulus.
- [By overstimulating some of your cone cells and then looking elsewhere](https://en.wikipedia.org/wiki/Impossible_color#Chimerical_colors) you can cause an "impossible" stimulus, one that no physical light could trigger on its own.

# Mathematics
> **Color is a 3-dimensional vector space.**

{{ image(asset="what-is-color/RGB_color_solid_cube.png", size="ml", by_title="RGB color solid cube", by_link="https://commons.wikimedia.org/wiki/File:RGB_color_solid_cube.png", by_author="SharkD", by_license="CC BY-SA 4.0") }}

Since humans only have three cone cells, all the different wavelengths in a color can be summarized with three different numbers. The amount that color activates our red cones, green cones (really ganglion cells), and blue cones! White is <col-s>[1, 1, 1]</col-s>, red is <col-s>[1, 0, 0]</col-s>, orange is <col-s>[1, 0.5, 0]</col-s>, and black is <col-s>[0, 0, 0]</col-s>.

A fancy thing you can do in math is rearrange numbers however you want. So instead of RGB we could use BGR where red is <col-s space="bgr">[0, 0, 1]</col-s>. Or we could use CMY where red is <col-s space="cmy">[0, 1, 1]</col-s>. Or we can store the [hue, saturation, and lightness (HSL)](https://en.wikipedia.org/wiki/HSL_and_HSV) so that red is <col-s space="hsl">[0, 1, 0.5]</col-s>. All these "color spaces" are interchangeable, so long as you clarify which you are using.

Depending on how you are using color, you may find some color spaces to be more useful than others. Computer generated imagery (CGI) tends to use RGB because it best represents the physics being simulated. But when encoding a video we [usually convert to YCbCr color space](https://trac.ffmpeg.org/wiki/colorspace) because it helps compress the differences in color that humans won't notice. If you need someone to pick a color, HSL is useful because it organizes everything into a color wheel. But HSL doesn't _quite_ match the biology of vision. For example, putting HSL's <col-s value="hsl(60deg, 100%, 60%)">yellow</col-s> next to its <col-s value="hsl(240deg, 100%, 60%)">blue</col-s>, the yellow looks far lighter despite having the same "lightness". [The oklab color space helps blend between colors more smoothly](https://bottosson.github.io/posts/oklab/).

# Engineering
> **Color is a signal sent to a display or received from a sensor.**

{{ image(asset="what-is-color/neptune_color.webp", size="ml", by_title="Modelling the seasonal cycle of Uranus’s colour and magnitude, and comparison with Neptune, Figure 8", by_link="https://doi.org/10.1093/mnras/stad3761", by_author="Patrick G J Irwin et al.", border="true") }}


Did you know that up until recently we did not know neptune's color? It was generally depicted as a <col-s value="#4766f2">vivid blue</col-s>, but that was just a guess. The planet was too far away from earth to see clearly through a telescope, and the sensor we sent on the Voyager 2 mission didn't measure the same wavelengths as the cones in your eye. Neptune is closer to <col-s value="#b4d7ea">white with just slight blue tint</col-s>!

It is an oft-repeated fact that NASA's cameras are black & white, but this isn't quite true. The magnetic deflection vidicon used as a camera on the Voyager missions couldn't tell frequencies apart by itself (for that it used a spectrometer). But [there were ten different color filters](https://en.wikipedia.org/wiki/Voyager_2#Scientific_instruments) Voyager could place in front of the vidicon to limit it to specific frequencies, including frequencies associated with methane and sodium. The Voyager spacecraft were built to do science, not take pretty pictures.

In fact, the [modern CMOS camera](https://en.wikipedia.org/wiki/Active-pixel_sensor) is also incapable of distinguishing different frequencies. Like Voyager, color filters are applied. But unlike Voyager, which could place any of the ten color filters in front of its camera, each pixel in your camera [has a single pigment permanently glued to the front of it](https://en.wikipedia.org/wiki/Color_filter_array). You get RGB by alternating red, green, and blue pixels. Then [some software makes up each of the two missing colors](https://docs.darktable.org/usermanual/4.6/en/module-reference/processing-modules/demosaic/) based on each pixel's neighbors. This makes life [famously difficult for people doing green screen work](https://youtu.be/aO3JgPUJ6iQ?si=zVltA3TjidHnoTKS).

Although Voyager's red, green, blue color filters were not calibrated to match the sensitivity of a human's cone cells, the magnetic deflection was calibrated to accurately measure light brightness. Unfortunately, the computer monitors on earth weren't. If the vidicon measured twice as much blue vs red, NASA's computers would simply double the power of the [electron beam illuminating the blue phosphors in each CRT monitor](https://en.wikipedia.org/wiki/Cathode-ray_tube). That increases the actual blue light output of the monitor by roughly 430%, so the image would look far too blue. NASA isn't stupid, but they were more interested in seeing the detail in Neptune's clouds than in perfect color reproduction. In some cases they even amplified the effect! For a photograph to look correct on a CRT monitor, [the colors need to be gamma corrected](https://en.wikipedia.org/wiki/Gamma_correction), so that "twice as bright" translates to only 137% as much power. Even now when CRTs are long gone, the sRGB color space used by this (and every other website) expects gamma corrected colors. And to this day, [lazy programers still screw it up](https://erikmcclure.com/blog/everyone-does-srgb-wrong-because/).

{{ svg(asset="what-is-color/gamut_comparison.svg", size="ml", by_title="CIE1931xy gamut comparison of sRGB P3 Rec2020" by_link="https://commons.wikimedia.org/wiki/File:CIE1931xy_gamut_comparison_of_sRGB_P3_Rec2020.svg", by_author="Myndex", by_license="CC BY-SA 4.0", by_mod="added dark theme support, optimized") }}

And that isn't the end of it. We can all agree (for example) that in sRGB color space <col-s>[0, 1, 0]</col-s> should produce the "greenest green". But what is the greenest green? For a long time it could vary wildly from one ~~computer~~TV to the next. Scientists did produce [an accurate color map of human vision way back in 1931](https://en.wikipedia.org/wiki/CIE_1931_color_space) (see above) giving precise x,&thinsp;y coordinates to every visible color, but [it wasn't until 1990 that the International Telecommunication Union got everyone to agree](https://www.itu.int/dms_pub/itu-r/opb/hdb/R-HDB-11-1989-PDF-E.pdf) the "greenest green" on a TV would have <col-s value="[0.3, 0.6, 0.0]" space="xyz">x,&thinsp;y of 0.3,&thinsp;0.6</col-s> based on a [1970 measurement of greenest phosphors](https://tech.ebu.ch/docs/tech/tech3213.pdf). [In 1996 Microsoft and HP adopted similar values](https://www.w3.org/Graphics/Color/sRGB.html). A pure <col-s>530nm</col-s> laser has x,&thinsp;y of roughly 0.8,&thinsp;0.15, but no monitor in 1996 or TV in 1970 could make a _green so extreme_, and so everyone settled for a more moderate green.

Of course, screens got better and monitors today _can_ display quite extreme greens. Alas, most software is limited to the sRGB gamut: all the colors that can be made by mixing the most red, most green, and most blue values in the sRGB color space (called the primaries). The P3 and Rec2020 color spaces have much wider gamuts, but at the time I'm writing this they aren't widely adopted. <col-s value="[0, 1, 0]" space="p3">P3's greenest green</col-s> and <col-s value="[0, 1, 0]" space="rec2020">Rec2020's greenest green</col-s> [will only look greener on a few devices](https://www.wide-gamut.com/).

# Psychology
> **Color is your brain guessing what pigments are in a thing.**

{{ image(asset="what-is-color/the_dress_diagram.png", size="ml", by_title="Wikipe-tan wearing The Dress", by_link="https://commons.wikimedia.org/wiki/File:Wikipe-tan_wearing_The_Dress_reduced.svg", by_author="Kasuga~jawiki", by_license="CC BY-SA 3.0", border="true") }}

Subconscious pigmentation solving is obvious in the dress picture above. Based on context, you correctly see the pigmentation of the apron on the left as <col-s value="#2138df">bright blue</col-s> and the pigmentation of the apron on the right as <col-s>white</col-s>, even though your eyes are actually sensing the same <col-s value="#948dbc">light brownish blue</col-s> light in each case. Your brain is measuring the effect of the <col-s value="#f3d39d">yellow</col-s> rectangle on the left and the <col-s value="#1b0b74">blue</col-s> rectangle on the right, and factoring it out. In the [original dress image the lighting was ambiguous](https://slate.com/technology/2017/04/heres-why-people-saw-the-dress-differently.html), so different brains solved the illumination problem differently.

Evolution doesn't care if you appreciate the beautiful blue of the noon sky, it only cares if that bush is a delicious wild blueberry or poisonous baneberry. Short of putting it in your mouth, how can you tell? By the pigments of course! The baneberry has few pigments, mostly reflecting whatever light hits it (that is, it is <col-s>white</col-s>). Whereas the blueberry absorbs most green wavelengths, reflecting blue and a bit of red (it is a <col-s value="#2a3f70">blue</col-s>berry). Great, so if your eyes sense more <col-s value="450nm">short wavelengths</col-s> than <col-s value="580nm">long/medium wavelengths</col-s>, it must be an edible blueberry! That, or you are looking at a <col-s>white</col-s> baneberry reflecting the light of that wonderful <col-s value="#83d5f7">blue</col-s> sky. Oh, and you'll starve when all the blueberries reflect the <col-s value="#f6c914">orange</col-s> illumination of a sunset and come out white. Oops.

So instead of taking light at face value, your brain subconsciously solves for the pigmentation of every object you see, taking into account the context you are seeing it in. Does the snow next to that blueberry also look blue? Then the light must be blue and the berry is white. Are the clouds flaming red? Then everything orangy is actually white, and blue things are _very blue_.

I've spend most of [PhD research](/matfusion) trying to get computers to separate pigmentation and lighting the way humans do without even noticing. It is insanely difficult. [For a while, researchers needed humans to manually rank the lightness of points so they could train matching computer systems](http://opensurfaces.cs.cornell.edu/intrinsic/). Brains are so cool 🧠.

Our built-in automatic pigmentation solving is a pain for people trying to display images on screens. Assuming you aren't [using an e-ink display](https://github.com/Modos-Labs/Glider), all the <col-s value="#b33772">colored squares</col-s> on this page are made of tiny glowing lights. But your brain wants to interpret them as pigmented paper illuminated by the lights of your office (or bathroom if my blog-reading habits are anything to go by). Your brain forcefully applies that context without you even noticing. If the lights of your room are a warm hue or the background of a page is off-white, then you'll perceive all the colors as slightly bluer (and vice-versa). People who need extremely accurate color sometimes [have a booth with exact D65 illumination](https://www.gtilite.com) (as [assumed by color spaces like sRGB](https://en.wikipedia.org/wiki/Standard_illuminant)) or a [monitor that adjusts its whitepoint based on ambient light](https://www.xrite.com/service-support/i1display-pro).

# Linguistics
> **Colors are words.**

{{ image(asset="what-is-color/colors_in_irish.png", size="ml", by_title="Colors in Irish", by_link="https://commons.wikimedia.org/wiki/File:Colours_in_Irish.png", by_author="Sherlyn", by_license="CC BY-SA 4.0", border="true") }}

TODO

# Art
> **Colors convey ideas.**

{{ image(asset="what-is-color/picasso_tragedy.jpg", size="ml", by_title="The Tragedy", by_author="Pablo Picasso", border="true") }}

TODO
