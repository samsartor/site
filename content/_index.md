+++
title = "Sam Sartor"
description = ""
sort_by = "date"
+++

{% tagline() %}
I research 3D scanning and graphics at William & Mary, when not distracted by programming languages and fiddle music!
{% end %}
