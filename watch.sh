#!/usr/bin/env sh

set -ex
#export NODE_OPTIONS=--no-experimental-fetch

yarn install
watchexec -w script $(yarn bin)/vite build --mode development &
zola serve --drafts
