# Cloudflare Build script

set -ex

# yarn install - install automatically runs
$(yarn bin)/vite build
zola build --drafts
